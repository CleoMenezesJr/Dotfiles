vim.g.mapleader = " "
local map = vim.api.nvim_set_keymap
map("n", "<C-A-h>", "<C-w>h", { noremap = true, silent = false })
map("n", "<C-A-l>", "<C-w>l", { noremap = true, silent = false })
map("n", "<C-A-j>", "<C-w>j", { noremap = true, silent = false })
map("n", "<C-A-k>", "<C-w>k", { noremap = true, silent = false })

-- map('i', 'jk', '<ESC>', {noremap = true, silent = false})
map("n", "<leader><TAB>", ":tabnext<CR>", { noremap = true, silent = false })
map("n", "<leader><s-TAB>", ":tabprevious<CR>", { noremap = true, silent = false })
-- barbar
map("n", "<A-,>", "<Cmd>BufferPrevious<CR>", { noremap = true, silent = false })
map("n", "<A-.>", "<Cmd>BufferNext<CR>", { noremap = true, silent = false })
-- not barbar
map("n", "<A-,>", "<Cmd>bp<CR>", { noremap = true, silent = false })
map("n", "<A-.>", "<Cmd>bn<CR>", { noremap = true, silent = false })

map("n", "<leader>e", ":NvimTreeToggle<CR>", { noremap = true, silent = true })
-- map("n", "<leader>q", ":Bdelete<CR>", { noremap = true, silent = true })

map("v", "<", "<gv", { noremap = true, silent = false })
map("v", ">", ">gv", { noremap = true, silent = false })

-- serchbox
map("n", "<leader>s", ":SearchBoxMatchAll<CR>", { noremap = true })
map("n", "<leader>S", ":SearchBoxReplace<CR>", { noremap = true })

vim.keymap.set("n", "<leader>mt", ":lua MiniMap.toggle()<CR>", { noremap = true, silent = false })
vim.keymap.set("t", "<esc>", "<c-\\><c-n>")

-- Flatpak

local function get_project_file()
	local path = vim.fn.expand("%:p:h") -- pega o diretório do arquivo atual
	local files = vim.fn.readdir(path) -- lista os arquivos do diretório

	for _, file in ipairs(files) do
		local _, _, ext = string.find(file, "^.+(%..+)$") -- extrai a extensão do arquivo
		local count = select(2, string.gsub(file, "%.", "")) -- conta o número de pontos no nome do arquivo

		if ext and (ext == ".json" or ext == ".yaml" or ext == ".yml") and count >= 3 then
			return path .. "/" .. file -- retorna o caminho completo do arquivo
		end
	end

	return nil -- retorna nil caso nenhum arquivo seja encontrado
end

function BuildProject()
	local project_file = get_project_file()
	if project_file then
		local command = string.format(
			"flatpak-spawn --host flatpak run org.flatpak.Builder build %s --user --install --force-clean && flatpak-spawn --host flatpak run %s",
			project_file,
			project_file
		)
		vim.api.nvim_command("!" .. command)
	else
		print("Nenhum arquivo de projeto encontrado")
	end
end

vim.api.nvim_set_keymap("n", "<leader>b", ":lua BuildProject()<CR>", { noremap = true })
