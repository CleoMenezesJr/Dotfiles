return require("packer").startup(function(use)
	-- Packer can manage itself.
	use("wbthomason/packer.nvim")

	-- lua `fork` of vim-web-devicons for neovim
	use("kyazdani42/nvim-web-devicons")

	-- A dark and light Neovim theme written in fennel, inspired by IBM Carbon.
	use({ "nyoom-engineering/oxocarbon.nvim" })

	-- OneDarkPro colorscheme.
	use("olimorris/onedarkpro.nvim")

	-- Adwaita colorscheme:
	use("Mofiqul/adwaita.nvim")
	--
	-- Dracula colorscheme:
	use("Mofiqul/dracula.nvim")

	-- I'm a Dracula addict.
	use("datamonsterr/nvim-dracula")

	-- Kanagawa colorscheme.
	use("rebelot/kanagawa.nvim")

	-- Github's Neovim themes
	use({ "projekt0n/github-nvim-theme" })

	-- A clean dark theme written in lua for neovim.
	use("tiagovla/tokyodark.nvim")

	-- Soothing pastel theme for (Neo)vim
	use({ "catppuccin/nvim", as = "catppuccin" })

	-- Treesitter configurations and abstraction layer for Neovim.
	use({ "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" })

	-- A blazing fast and easy to configure Neovim statusline written in Lua.
	use({
		"nvim-lualine/lualine.nvim",
		requires = { "kyazdani42/nvim-web-devicons", opt = true },
	})
	-- Delete Neovim buffers without losing window layout
	use("famiu/bufdelete.nvim")

	-- A file explorer tree for neovim written in lua
	use({
		"nvim-tree/nvim-tree.lua",
		requires = {
			"nvim-tree/nvim-web-devicons", -- optional, for file icons
		},
		tag = "nightly", -- optional, updated every week. (see issue #1193)
	})

	-- Use treesitter to autoclose and autorename html tag.
	use("windwp/nvim-ts-autotag")

	-- Rainbow parentheses for neovim using tree-sitter.
	use("HiPhish/nvim-ts-rainbow2")

	-- A super powerful autopair plugin for Neovim that supports multiple characters.
	use("windwp/nvim-autopairs")

	-- Create key bindings that stick.
	use("folke/which-key.nvim")

	-- Create key bindings that stick.
	use({
		"nvim-telescope/telescope.nvim",
		requires = { { "nvim-lua/plenary.nvim" } },
	})

	-- Completion.
	-- Quickstart configs for Nvim LSP.
	use("neovim/nvim-lspconfig")
	use("hrsh7th/cmp-nvim-lsp")
	use("hrsh7th/cmp-buffer")
	use("hrsh7th/cmp-path")
	use("hrsh7th/cmp-vsnip")
	use("hrsh7th/vim-vsnip")
	use("hrsh7th/cmp-cmdline")
	use("rafamadriz/friendly-snippets")
	-- A completion plugin for neovim coded in Lua.
	use("hrsh7th/nvim-cmp")
	use("onsails/lspkind.nvim")
	-- vscode-like pictograms for neovim lsp completion items.
	-- Neovim plugin that allow you to seamlessly manage LSP servers.
	use({ "williamboman/mason.nvim" })
	use({ "williamboman/mason-lspconfig.nvim" })
	use("lewis6991/gitsigns.nvim")

	-- Indent guides for Neovim.
	use("lukas-reineke/indent-blankline.nvim")

	-- Start your search from a more comfortable place, say the upper right corner?
	use({
		"VonHeikemen/searchbox.nvim",
		requires = {
			{ "MunifTanjim/nui.nvim" },
		},
	})

	-- Format.
	use({ "jose-elias-alvarez/null-ls.nvim" })

	-- Neovim dev container support
	use("https://codeberg.org/esensar/nvim-dev-container")

	--markdown preview plugin for (neo)vim.
	use({
		"iamcco/markdown-preview.nvim",
		run = function()
			vim.fn["mkdp#util#install"]()
		end,
	})

	-- Develop inside docker containers, just like VSCode.
	use("jamestthompson3/nvim-remote-containers")

	-- multi-plugins mini.nvim
	use("echasnovski/mini.nvim")

	-- Highlight arguments' definitions and usages, using Treesitter
	use({
		"m-demare/hlargs.nvim",
		requires = { "nvim-treesitter/nvim-treesitter" },
	})
	-- Highlight colors for neovim
	use("brenoprata10/nvim-highlight-colors")
	-- A plugin to visualise and resolve merge conflicts in neovim
	use({
		"akinsho/git-conflict.nvim",
		tag = "*",
		config = function()
			require("git-conflict").setup()
		end,
	})
	-- A complete plugin for moving and duplicating blocks and lines, with complete fold handling, reindenting, and undoing in one go.
	use("booperlv/nvim-gomove")
	-- A plugin for neovim that highlights cursor words and lines
	use("yamatsum/nvim-cursorline")
	-- Show code context
	use({
		"nvim-treesitter/nvim-treesitter-context",
		config = function()
			require("treesitter-context").setup()
		end,
	})

	-- Blueprint sintax highlight
	use("thetek42/vim-blueprint-syntax")
	use("https://gitlab.com/gabmus/vim-blueprint")

	-- Not UFO in the sky, but an ultra fold in Neovim.
	use({ "kevinhwang91/nvim-ufo", requires = "kevinhwang91/promise-async" })

	-- Distraction-free coding for Neovim
	use({ "folke/zen-mode.nvim" })
	-- A VS Code like winbar for Neovim
	use({
		"utilyre/barbecue.nvim",
		tag = "*",
		-- branch = "fix/E36",
		requires = {
			"neovim/nvim-lspconfig",
			"SmiteshP/nvim-navic",
			"nvim-tree/nvim-web-devicons", -- optional dependency
		},
		-- after = "nvim-web-devicons", -- keep this if you're using NvChad
		config = function()
			require("barbecue").setup()
		end,
	})
	-- A quick and dirty WPM calculator for your UI needs
	use({
		"jcdickinson/wpm.nvim",
		config = function()
			require("wpm").setup({})
		end,
	})
	-- Manage containers, images and compose files with no effort
	use({ "lpoto/telescope-docker.nvim" })
	-- Easily sort Tailwind classes in Neovim.
	use({
		"laytan/tailwind-sorter.nvim",
		requires = { "nvim-treesitter/nvim-treesitter", "nvim-lua/plenary.nvim" },
		config = function()
			require("tailwind-sorter").setup()
		end,
		run = "cd formatter && npm i && npm run build",
	})
	-- Automatically expand width of the current window. Maximizes and restore it. And all this with nice animations!
	use({
		"anuvyklack/windows.nvim",
		requires = {
			"anuvyklack/middleclass",
			"anuvyklack/animation.nvim",
		},
	})
	-- Neovim treesitter plugin for setting the commentstring based on the cursor location in a file.
	use("JoosepAlviste/nvim-ts-context-commentstring")
	-- The neovim tabline plugin.
	-- use({ "romgrk/barbar.nvim", requires = "nvim-web-devicons" })
	-- changing the way of using tabs on neovim. Tab::Buf.
	use({
		"tiagovla/scope.nvim",
		config = function()
			require("scope").setup()
		end,
	})
	use("luukvbaal/statuscol.nvim")
end)
