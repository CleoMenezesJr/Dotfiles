local null_ls = require("null-ls")

local formatting = null_ls.builtins.formatting

local sources = {
	formatting.isort,
	formatting.black.with({ extra_args = { "--line-length", "79", "safe" } }),
	formatting.djlint,
	formatting.stylua,
	formatting.xmllint,
	formatting.stylelint,
  formatting.prettier,
}

null_ls.setup({
	sources = sources,
})
