-- vim.cmd([[highlight IndentBlanklineIndent1 guifg=#E06C75 gui=nocombine]])
vim.api.nvim_set_hl(0, "IndentBlanklineIndent1", { fg = vim.g.terminal_color_1 or "#E06C75" })
vim.api.nvim_set_hl(0, "IndentBlanklineIndent2", { fg = vim.g.terminal_color_2 or "#E5C07B" })
vim.api.nvim_set_hl(0, "IndentBlanklineIndent3", { fg = vim.g.terminal_color_3 or "#98C379" })
vim.api.nvim_set_hl(0, "IndentBlanklineIndent4", { fg = vim.g.terminal_color_4 or "#56B6C2" })
vim.api.nvim_set_hl(0, "IndentBlanklineIndent5", { fg = vim.g.terminal_color_5 or "#61AFEF" })
vim.api.nvim_set_hl(0, "IndentBlanklineIndent6", { fg = vim.g.terminal_color_6 or "#C678DD" })
require("indent_blankline").setup({
	space_char_blankline = " ",
	show_current_context = true,
	-- show_current_context_start = true,
	-- char_highlight_list = {
	-- 	"IndentBlanklineIndent1",
	-- 	"IndentBlanklineIndent2",
	-- 	"IndentBlanklineIndent3",
	-- 	"IndentBlanklineIndent4",
	-- 	"IndentBlanklineIndent5",
	-- 	"IndentBlanklineIndent6",
	-- },
	space_char_highlight_list = {
		"IndentBlanklineIndent1",
		"IndentBlanklineIndent2",
		"IndentBlanklineIndent3",
		"IndentBlanklineIndent4",
		"IndentBlanklineIndent5",
		"IndentBlanklineIndent6",
	},
	-- show_trailing_blankline_indent = true,
})
