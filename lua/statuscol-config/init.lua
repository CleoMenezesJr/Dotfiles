local builtin = require("statuscol.builtin")
require("statuscol").setup({
	relculright = false,
	-- thousands = false,
	ft_ignore = { "nvim-tree" },
	segments = {
		{
			sign = { name = { "Diagnostic" }, maxwidth = 2, auto = true },
			condition = { true, builtin.not_empty },
			click = "v:lua.ScSa",
		},
    { text = { builtin.foldfunc, "" }, click = "v:lua.ScFa" },
		{
			text = { builtin.lnumfunc, "" },
			-- condition = { true, builtin.not_empty },
			click = "v:lua.ScLa",
		},
		{
			sign = { name = { ".*" }, maxwidth = 2, colwidth = 1, auto = false },
			-- condition = { true, builtin.not_empty },
			click = "v:lua.ScSa",
		},
	},
})
