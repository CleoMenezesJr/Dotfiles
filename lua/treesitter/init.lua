require("nvim-treesitter.configs").setup({
	ensure_installed = { "c", "lua", "rust", "python", "html", "javascript", "css" },
	auto_install = true,
	indent = {
		enable = true,
	},
	highlight = {
		enable = true,
		additional_vim_regex_highlighting = false,
	},
	autotag = {
		enable = true,
	},
	rainbow = {
		enable = true,
		-- list of languages you want to disable the plugin for
		-- disable = { "jsx", "cpp" },
		-- Which query to use for finding delimiters
		query = "rainbow-parens",
		-- Highlight the entire buffer all at once
		strategy = require("ts-rainbow").strategy.global,
		autopairs = {
			enable = true,
		},
	},
	context_commentstring = {
		enable = true,
		config = {
			astro = "<!-- %s -->",
		},
	},
})
