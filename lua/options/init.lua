vim.o.number = true
vim.o.cursorline = true
vim.o.title = true
vim.o.tabstop = 4
vim.bo.tabstop = 4
vim.o.shiftwidth = 4
vim.bo.shiftwidth = 4
vim.o.softtabstop = 4
vim.bo.softtabstop = 4
vim.o.autoindent = true
vim.bo.autoindent = true
vim.o.expandtab = true
vim.bo.expandtab = true
vim.o.shortmess = vim.o.shortmess .. "c"
vim.o.hidden = true
vim.o.whichwrap = "b,s,<,>,[,],h,l"
-- vim.o.pumheight = 10
vim.o.fileencoding = "utf-8"
vim.o.cmdheight = 1
vim.o.splitbelow = true
vim.o.splitright = true
vim.opt.termguicolors = true
-- vim.o.conceallevel = 0
vim.o.showtabline = true
vim.o.showmode = false
-- vim.o.backup = false
vim.o.writebackup = false
vim.o.updatetime = 300
vim.o.timeoutlen = 100
-- vim.o.hlsearch = false
vim.o.ignorecase = true
vim.o.scrolloff = 7
vim.o.sidescrolloff = 9
vim.o.mouse = "a"
vim.wo.wrap = false
vim.wo.signcolumn = "no"
vim.opt.completeopt = "menuone,noselect"
vim.o.clipboard = "unnamedplus"
vim.cmd("set noswapfile")
vim.opt.fillchars:append("eob: ") -- remove end buffer
vim.o.showmatch = true
vim.opt.display:append({ "lastline" })
vim.opt.smartindent = false

vim.o.foldcolumn = "1" -- '0' is not bad
vim.o.foldlevel = 99 -- Using ufo provider need a large value, feel free to decrease the value
vim.o.foldlevelstart = 99
vim.o.foldenable = true
vim.o.fillchars = [[eob: ,fold: ,foldopen:,foldsep: ,foldclose:]]

-- Indent blankline
vim.opt.list = true
vim.opt.listchars:append("space:⋅")
-- vim.opt.listchars:append("eol:↴")

-- Windows.nvim
vim.o.winwidth = 10
vim.o.winminwidth = 10
vim.o.equalalways = false

-- vim.cmd("colorscheme catppuccin-mocha")
vim.cmd("colorscheme onedark")
vim.api.nvim_set_hl(0, "SignColumn", { bg = "NONE" })
-- One dark
-- vim.cmd([[hi NvimTreeFolderIcon guifg=#61AFEE ctermbg=NONE]])
-- vim.cmd([[hi TreesitterContext guibg=#242424 ctermbg=NONE]])
-- vim.cmd([[hi TreesitterContextLineNumber guibg=#242424 ctermbg=NONE]])

-- vim.cmd[[hi NvimTreeWinSeparator guibg=NONE ctermbg=NONE]]
-- vim.cmd([[hi NvimTreeNormal guibg=#161616 ctermbg=NONE]])
-- vim.cmd([[hi NvimTreeNormalNC guibg=#161616 ctermbg=NONE]])

-- vim.cmd([[hi CursorLine guibg=#292929 ctermbg=NONE]])
-- vim.cmd([[hi BufferLineTabSelected guifg=#303030 ctermbg=NONE]])

-- vim.opt.shell = "flatpak-spawn --host --env=TERM=xterm-256color zsh"
vim.opt.shell = "bash"
-- vim.opt.shellcmdflag =
-- 	"-NoLogo -NoProfile -ExecutionPolicy RemoteSigned -Command [Console]::InputEncoding=[Console]::OutputEncoding=[System.Text.Encoding]::UTF8;"
-- vim.opt.shellredir = "-RedirectStandardOutput %s -NoNewWindow -Wait"
-- vim.opt.shellpipe = "2>&1 | Out-File -Encoding UTF8 %s; exit $LastExitCode"
-- vim.opt.shellquote = ""
-- vim.opt.shellxquote = ""

vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
