local actions = require("telescope.actions")
-- local trouble = require("trouble.providers.telescope")
require("telescope").setup({
	defaults = {
		layout_config = {
			width = 0.75,
			prompt_position = "bottom",
			preview_cutoff = 120,
			horizontal = { mirror = false },
			vertical = { mirror = false },
		},
		find_command = { "rg", "--no-heading", "--with-filename", "--line-number", "--column", "--smart-case" },
		prompt_prefix = " ",
		selection_caret = " ",
		entry_prefix = "  ",
		initial_mode = "insert",
		selection_strategy = "reset",
		sorting_strategy = "ascending",
		layout_strategy = "horizontal",
		file_sorter = require("telescope.sorters").get_fuzzy_file,
		file_ignore_patterns = {},
		generic_sorter = require("telescope.sorters").get_generic_fuzzy_sorter,
		path_display = {},
		winblend = 0,
		border = {},
		borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
		color_devicons = true,
		use_less = true,
		set_env = { ["COLORTERM"] = "truecolor" }, -- default = nil,
		file_previewer = require("telescope.previewers").vim_buffer_cat.new,
		grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,
		qflist_previewer = require("telescope.previewers").vim_buffer_qflist.new,
		buffer_previewer_maker = require("telescope.previewers").buffer_previewer_maker,
		mappings = {
			i = {
				["<C-j>"] = actions.move_selection_next,
				["<C-k>"] = actions.move_selection_previous,
				["<C-q>"] = actions.smart_send_to_qflist + actions.open_qflist,
				["<esc>"] = actions.close,
				["<CR>"] = actions.select_default + actions.center,
				-- ["<c-t>"] = trouble.open_with_trouble,
			},
			n = {
				["<C-j>"] = actions.move_selection_next,
				["<C-k>"] = actions.move_selection_previous,
				["<C-q>"] = actions.smart_send_to_qflist + actions.open_qflist,
				-- ["<c-t>"] = trouble.open_with_trouble,
			},
		},
	},
	extensions = {
		-- NOTE: this setup is optional
		docker = {
			-- theme = "ivy",
			binary = "podman-remote", -- in case you want  to use podman or something
			log_level = vim.log.levels.INFO,
			-- init_term = "tabnew", -- "vsplit new", "split new", ...
			-- NOTE: init_term may also be a function that receives
			-- a command and a table of env. variables as input.
			-- This is intended only for advanced use, in case you want
			-- to send the env. and command to a tmux terminal or floaterm
			-- or something other than a built in terminal.
		},
	},
})

-- Load the docker telescope extension
require("telescope").load_extension("docker")
